package pandey.sudeep.room;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    private List<Book> books;

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;

        public ViewHolder(CardView cv){
            super(cv);
            this.cardView=cv;
        }
    }

    public BooksAdapter(List<Book> _books){
        this.books=_books;
    }

    @Override
    public int getItemCount(){
        return books.size();
    }

    @Override
    public BooksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final CardView cardView = holder.cardView;

        final TextView textView1 = (TextView) cardView.findViewById(R.id.textView1);
        final TextView textView2 = (TextView) cardView.findViewById(R.id.textView2);

        textView1.setText(books.get(position).getTitle());
        textView2.setText((books.get(position).getPublishedDate().toString()));

    }
}
