package pandey.sudeep.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface MyDao {

    @Insert
    public long[] insertBooks(Book... books);

    @Insert
    public long[] insertAuthors(Author... authors);

    //The Update convenience method modifies a set of entities, given as parameters, in the database.
    //It uses a query that matches against the primary key of each entity.
    @Update
    public int updateAuthors(Author... authors);

    //The Delete convenience method removes a set of entities, given as parameters, from the database.
    //It uses the primary keys to find the entities to delete.
    @Delete
    public int deleteBooks(Book... books);

    @Delete
    public int deleteAuthors(Author... authors);


    @Query("Select * from books")
    public Book[] retrieveBooks();


    @Query("Select * from writers")
    public Author[] retrieveAuthors();


}
