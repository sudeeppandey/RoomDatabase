package pandey.sudeep.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "writers",indices={@Index(value={"bookID"})},foreignKeys = @ForeignKey(entity = Book.class,parentColumns = "id",childColumns = "bookID",onDelete = CASCADE))
public class Author {

    @PrimaryKey(autoGenerate = true)
    private int authorId;

    private int bookID;

    @ColumnInfo(name = "given_name")
    private String firstName;

    @ColumnInfo(name = "surname")
    private String lastName;


    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Author(){

    }

    public Author(int _bookID, String _firstName, String _lastName){

        this.bookID=_bookID;
        this.firstName=_firstName;
        this.lastName=_lastName;
    }
}
