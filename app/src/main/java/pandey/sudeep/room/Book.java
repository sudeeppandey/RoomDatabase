package pandey.sudeep.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "books",indices={@Index(value={"book_title"})})
public class Book {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "book_title")
    public String title;

    @ColumnInfo(name = "released_date")
    public Date publishedDate;

    public Book(){

    }

    public Book(String _title, Date _publishedDate){
        this.title=_title;
        this.publishedDate=_publishedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }
}
