package pandey.sudeep.room;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.Arrays;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);;
    private BooksAdapter booksAdapter;
    private AuthorsAdapter authorsAdapter;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Book[] books = new Book[5];
        books[0]=(new Book("Black Pearl",new Date(10000000000L)));
        books[1]=(new Book("Strangers Tides",new Date(5000000000000L)));
        books[2]=(new Book("Dead Man's Chest",new Date(6000000000000L)));
        books[3]=(new Book("At World's End",new Date(80000000000L)));
        books[4]=(new Book("Dead Men Tell No Tales",new Date(90000000000L)));
        AppDatabase.getAppDatabase(context).fetchDao().insertBooks(books);

        Author[] authors = new Author[5];
        authors[0]=(new Author(1,"Jack","Sparrow"));
        authors[1]=(new Author(2,"Davy","Jones"));
        authors[2]=(new Author(3,"Elizabeth","Swann"));
        authors[3]=(new Author(4,"Will","Turner"));
        authors[4]=(new Author(5,"Hector","Barbossa"));
        AppDatabase.getAppDatabase(context).fetchDao().insertAuthors(authors);

        Button books_Button = (Button)findViewById(R.id.button4);
        books_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerView=findViewById(R.id.rView);
                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                recyclerView.setHasFixedSize(true);

                recyclerView.setLayoutManager(linearLayoutManager);
                //instantiate booksAdapter
                booksAdapter=new BooksAdapter(Arrays.asList(AppDatabase.getAppDatabase(context).fetchDao().retrieveBooks()));
                recyclerView.setAdapter(booksAdapter);
            }
        });

        Button authors_Button = (Button)findViewById(R.id.button5);
        authors_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerView=findViewById(R.id.rView);
                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                //instantiate authorsAdapter
                authorsAdapter=new AuthorsAdapter(Arrays.asList(AppDatabase.getAppDatabase(context).fetchDao().retrieveAuthors()));
                recyclerView.setAdapter(authorsAdapter);
            }
        });
    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }
}
