package pandey.sudeep.room;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AuthorsAdapter extends RecyclerView.Adapter<AuthorsAdapter.ViewHolder> {

    private List<Author> authors;

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;

        public ViewHolder(CardView cv){
            super(cv);
            this.cardView=cv;
        }
    }

    public AuthorsAdapter(List<Author> _authors){
        this.authors=_authors;
    }

    @Override
    public int getItemCount(){
        return authors.size();
    }

    @Override
    public AuthorsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        AuthorsAdapter.ViewHolder vh = new AuthorsAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AuthorsAdapter.ViewHolder holder, final int position) {

        final CardView cardView = holder.cardView;

        final TextView textView1 = (TextView) cardView.findViewById(R.id.textView1);
        final TextView textView2 = (TextView) cardView.findViewById(R.id.textView2);

        textView1.setText(authors.get(position).getFirstName());
        textView2.setText((authors.get(position).getLastName()));

    }

}
